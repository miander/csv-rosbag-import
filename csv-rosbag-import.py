'''
Copyright (c) 2016 Michael Anderson

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.


                         CSV to rosbag import tool v1
'''

import rosbag, sys, csv, re, rospy, subprocess, random
from std_msgs.msg import Int32, String, Float32, Time
from datetime import datetime
#from dateutil import parser

'''
 Usage: python csvtobag.py infile.csv outfile.bag

 Overall design: Start by reading the first (header) row and generate ROS
 topics for each, except for the timestamp column which be copied to the
 timestamp field of each message. The first row of data (after the header)
 will be used to determine the data types of the columns. As we insert fields
 into the bag we will cast them to the respective types we determined for them.
''' 

def slugify(value):
    """
    Converts string to lowercase, removes non-alpha characters, and convert
    spaces to hyphens. This function is adapted from the Django Project.
    https://stackoverflow.com/questions/295135/turn-a-string-into-a-valid-filename-in-python
    """
    value = re.sub('[^\w\s-]', '', value).strip().lower()
    value = re.sub('[-\s]+', '-', value)
    return value

def parsedate(string):
    # Parse is a bit overzealous... It thinks most strings are dates.
    # return parser.parse(string)

    # This means you might have to tweak the date format string for your csv.
    # The format string used here matches this format: 02/08/2016 13:14:02.583
    return datetime.strptime(string, "%m/%d/%Y %H:%M:%S.%f")

def setzero(date):
    # Set the t-zero value used by the rosdate functions below.
    global rosdatezero
    rosdatezero = date

def rospydate(string):
    # Make a nice datetime then convert it to ROS' weird relative time format.
    assert 'rosdatezero' in globals()
    date = parsedate(string)
    td = date - rosdatezero
    nsec = td.microseconds * 1000
    sec = td.seconds + (td.days * 24 * 3600)
    return rospy.Time(sec, nsec)
    
def rosdate(string):
    # Same thing but converted to the OTHER ros time type (std_msgs.Time)
    # because for some insane reason we need the rospy type for the timestamps
    # and the std_msgs type for the message data.
    return Time(rospydate(string))

def parsefloat(string):
    # This needs its own function too because the ROS Float32 cast is really
    # greedy for some reason and testing for type compatibility by casting a
    # string to it never fails...
    return Float32(float(string))

# Code starts here
with open(sys.argv[1], 'rb') as csvf, rosbag.Bag(sys.argv[2], 'w') as bagf:
    reader = csv.reader(csvf)
    
    # Generate list of topic names representing the columns
    # First, get the csv header
    header = next(reader)
    # loop through the header entries and convert them to topic names
    topics = []
    columniter = iter(header)
    for columnname in header:
        topics.append('/' + slugify(columnname))
    
    # Here we encounter the first the major sticking point of this process.
    # Because ROS stores times relative to the start of the roscore instead of
    # real dates, we have to choose a t=0 and subtract it from all of our other
    # time stamps as we go. Here we will assume that the first data row's time
    # stamp precedes the rest of the data's. If this is not the case you will
    # have some data at negative times but it shouldn't actually break
    # anything.
    firstline = next(reader)
    setzero(parsedate(firstline[0]))
    
    # Determine column data types, currently supporting
    # float, date, str. There's probably a more Pythonic way of doing
    # this. Check float first, then date. If both fail, use str by default.
    columntypes = [String] * len(header)
    # By using the parsedate function as a "type" we can use it to cast to
    # a datetime object on the fly while we're inserting data into the bag.
    # Due to ROS being silly we have to do the same thing with the floats
    # (see above).
    supportedtypes = [parsefloat, rosdate]
    # Test supported types against each field
    for idx,field in enumerate(firstline):
        for t in supportedtypes:
            try:
                # Try to cast
                t(field)
                # If there was no exception, the cast succeeded; note the type.
                columntypes[idx] = t
                break
            except ValueError:
                # Try the next type
                continue
           
    # We have all of the information we need to start parsing. We already
    # iterated our reader to get the first row so it's easiest to remake it.
    csvf.seek(0)
    reader = csv.reader(csvf)
    next(reader) # Skip header
    
    for rownum, row in enumerate(reader):
        try:
            # Show some basic progress info
            if (rownum % 50 == 0):
                print 'Row ' + str(rownum)
            # Grab the timestamp first
            timestamp = rospydate(row[0])
            # Finally, cast each field to its final data type and add it to the
            # bag with the time stamp.
            for colnum, field in enumerate(row):
                bagf.write(topics[colnum], columntypes[colnum](field), timestamp)
        except:
            # Report an error and skip the row
            print 'Error reading csv line ' + str(rownum)
    
# How about some nice info about the output?
cmd = ['rosbag', 'info', sys.argv[2]]
subprocess.Popen(cmd)